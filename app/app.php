<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Paste\Exception\PasteNotFoundException;

require __DIR__ . '/bootstrap.php';

// App parameters
$parameters = require_once __DIR__ . '/config/parameters';

$app = new Application();

if ($parameters['debug']) {
	$app['debug'] = true;
}

// 
$app->error(function(PasteNotFoundException $e) {
	return new Response('404 Paste Tidak Ditemukan');
});

//
require __DIR__ . '/config/resource.php';

// 
require __DIR__ . '/config/routes.php';

return $app;