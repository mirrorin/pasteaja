<?php

use Symfony\Component\HttpFoundation\Request;

$app = require __DIR__ . '/../app/app.php';

if (isset($argv) && count($argv) > 0) {
	// 
	list($_, $methode, $path) = $argv;
	$request = Request::create($path, $methode);
	$app->run($request);
} else {
	//
	$app->run();
}